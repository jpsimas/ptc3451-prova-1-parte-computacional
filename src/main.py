# PTC3451-comp Solution to Computational Part of First Exam of PTC3451
# Copyright (C) 2021  João Pedro de Omena Simas

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# required directory structure
# ./data - contains wav files entradaP1.wav e desejadoP1.wav
# ./src - contains this file
# ./img - directory where images are stored (needs to be created
# before running this program) 
# ./out - directory where generated wav files are stored

import numpy as np
from math import pi
import scipy.signal
import scipy.io.wavfile
import abc

import matplotlib.pyplot as plt

class transversalFilter(metaclass = abc.ABCMeta):
    def __init__(self, nW, mu, w_initial = None):
        if(w_initial != None):
            self._w = w_initial
        else:
            self._w = np.zeros(nW)
        self._mu = mu
    def getW(self):
        return self._w
    def reset(self):
        self._w = np.zeros(self._w.size)
    @abc.abstractmethod
    def update(self, d, u):
        pass

class lmsFilter(transversalFilter):
    def update(self, d, u):
        y = self._w.dot(u)
        e = d - y
        self._w += self._mu*e*u
        return self._w, y, e

class nlmsFilter(transversalFilter):
    def __init__(self, nW, mu, w_initial = None, delta = 1e-6):
        self._delta = delta
        super().__init__(nW, mu, w_initial)
        
    def update(self, d, u):
        y = self._w.dot(u)
        e = d - y
        self._w += self._mu*e*u/(u.dot(u) + self._delta)
        return self._w, y, e

class rlsFilter(transversalFilter):
    def __init__(self, nW, lamb, w_initial = None, delta = 1):
        self._P = np.eye(nW)/delta
        self._delta = delta
        super().__init__(nW, lamb, w_initial)

    def reset(self):
        self._P = np.eye(self._w.size)/self._delta
        super().reset()
        
    def update(self, d, u):
        self._P /= self._mu

        un = np.matmul(u, self._P)
    
        mui = 1/(1 + np.dot(u, un))
        
        y = self._w.dot(u)
        e = d - y
        
        self._w += (mui*un)*e

        #if you do the multiplication mui un beforehand this matrix is
        #not guaranteed to be symmetric due to rounding errors...
        self._P -= mui*(np.asmatrix(un).H*(un))
        
        return self._w, y, e
    
def runSim(nW, mu, u, d, filter_t = nlmsFilter):

    n = u.size
    
    MSE = np.zeros(n - nW)
    EMSE = np.zeros(n - nW)

    adap = filter_t(nW, mu)

    w = np.zeros((n - nW, nW))
    
    e = np.zeros(n - nW)
    
    for i in range(0, n - nW):
        ui = u[i:(i + nW)]
        di = d[i + nW - 1]

        wi, y, e[i] = adap.update(di, ui)
            
        #note that, for the sake of simplicity, w is inverted
        w[i, :] = wi
            
    MSE = e*e
        
    return w, e, MSE, (d[(nW - 1):(-1)] - e)

def runSims(nW, mus, u, d, name = ""):
    MSEss_nlms = np.zeros(len(mus))
    MSEss_lms = np.zeros(len(mus))
    MSEss_rls = np.zeros(len(mus))
    
    for k in range(0, len(mus)):
        print("Running simulations with $\\mu = {}$".format(mus[k]))
        mu = mus[k]
        print("Simulating NLMS")
        w_nlms, e_nlms, MSE_nlms, y_nlms = runSim(nW, mu, u, d, filter_t = nlmsFilter)
        scipy.io.wavfile.write("../out/y_" + "nlms" + str(k) + "_" + str(nW) +".wav", 11025, y_nlms)
        print("Simulating LMS")
        w_lms, e_lms, MSE_lms, y_lms = runSim(nW, mu/nW, u, d, filter_t = lmsFilter)
        scipy.io.wavfile.write("../out/y_" + "lms" + str(k) + "_" + str(nW) +".wav", 11025, y_lms)
        print("Simulating RLS")
        w_rls, e_rls, MSE_rls,  y_rls = runSim(nW, 1 - mu/nW, u, d, filter_t = rlsFilter)
        scipy.io.wavfile.write("../out/y_" + "rls" + str(k) + "_" + str(nW) +".wav", 11025, y_rls)
        
        print("Done.")
        MSEss_nlms[k] = np.mean(MSE_nlms[-100:])
        print("MSEss_nlms = {}".format(10*np.log10(MSEss_nlms[k])) + " dB")
        t_conv_nlms = np.argmax(MSE_nlms < MSEss_nlms[k]*2)
        print("t_conv_nlms = {}".format(t_conv_nlms))
        
        MSEss_lms[k] = np.mean(MSE_lms[-100:])
        print("MSEss_lms = {}".format(10*np.log10(MSEss_lms[k])) + " dB")
        t_conv_lms = np.argmax(MSE_lms < MSEss_lms[k]*2)
        print("t_conv_lms = {}".format(t_conv_lms))

        MSEss_rls[k] = np.mean(MSE_rls[-100:])
        print("MSEss_rls = {}".format(10*np.log10(MSEss_rls[k])) + " dB")
        t_conv_rls = np.argmax(MSE_rls < MSEss_rls[k]*2)
        print("t_conv_rls = {}".format(t_conv_rls))
        
        #MSEtheo = mu * sigma_v * sigma_v /(2 - mu) + sigma_v*sigma_v

        #print("MSE_theo = {} dB".format(10*np.log10(MSEtheo)))
        
        plt.figure(1)
        ax = plt.subplot(len(mus), 1, k + 1)
        ax.set_title("$\\mu_{{NLMS}} = {:.2e}$".format(mu))
        plt.plot(10*np.log10(MSE_nlms), label = "NLMS" + ". $\\mu = {:.2e}$".format(mu), alpha = 0.5)
        plt.plot(10*np.log10(MSE_lms), label = "LMS" + ". $\\mu = {:.2e}$".format(mu/nW), alpha = 0.5)
        plt.plot(10*np.log10(MSE_rls), label = "RLS" + ". $\\lambda = 1 - {:.2e}$".format(mu/nW), alpha = 0.5)
        plt.xlabel("Discrete Time")
        plt.ylabel("SE (dB)")
        plt.ylim((-200, 0))
        plt.legend()

        plt.figure(2)
        ax = plt.subplot(len(mus), 3, 3*k + 0 + 1)
        ax.set_title("LMS" + ". $\\mu = {:.2e}$".format(mu/nW))
        plt.plot(u, label = "u(i)")
        plt.plot(e_lms, label = "e(i)")
        plt.xlabel("Discrete Time")
        plt.ylim((-1, 1))
        ax = plt.subplot(len(mus), 3, 3*k + 1 + 1)
        ax.set_title("NLMS" + ". $\\mu = {:.2e}$".format(mu))
        plt.plot(u, label = "u(i)")
        plt.plot(e_nlms, label = "e(i)")
        plt.xlabel("Discrete Time")
        plt.ylim((-1, 1))
        ax = plt.subplot(len(mus), 3, 3*k + 2 + 1)
        ax.set_title("RLS" + ". $\\lambda = 1 - {:.2e}$".format(mu/nW))
        plt.plot(u, label = "u(i)")
        plt.plot(e_rls, label = "e(i)")
        plt.xlabel("Discrete Time")
        plt.ylim((-1, 1))

        plt.figure(5)
        ax = plt.subplot(len(mus), 3, 3*k + 0 + 1)
        ax.set_title("LMS" + ". $\\mu = {:.2e}$".format(mu/nW))
        plt.plot(d, label = "d(i)")
        plt.plot(y_lms, label = "y(i)")
        plt.xlabel("Discrete Time")
        plt.ylim((-1, 1))
        ax = plt.subplot(len(mus), 3, 3*k + 1 + 1)
        ax.set_title("NLMS" + ". $\\mu = {:.2e}$".format(mu))
        plt.plot(d, label = "d(i)")
        plt.plot(y_nlms, label = "y(i)")
        plt.xlabel("Discrete Time")
        plt.ylim((-1, 1))
        ax = plt.subplot(len(mus), 3, 3*k + 2 + 1)
        ax.set_title("RLS" + ". $\\lambda = 1 - {:.2e}$".format(mu/nW))
        plt.plot(d, label = "d(i)")
        plt.plot(y_rls, label = "y(i)")
        plt.xlabel("Discrete Time")
        plt.ylim((-1, 1))

    plt.figure(1)
    plt.tight_layout()
    plt.savefig("../img/MSE" + name + ".svg")
    
    plt.figure(2)
    plt.tight_layout()
    plt.legend()
    plt.savefig("../img/sigs_eu" + name + ".svg")

    plt.figure(5)
    plt.tight_layout()
    plt.legend()    
    plt.savefig("../img/sigs_yd" + name + ".svg")
    
    #return MSEss_rls, w_rls

def main():
    print("PTC3451-comp  Copyright (C) 2021  João Pedro de O. Simas\n"\
          "This program comes with ABSOLUTELY NO WARRANTY.\n"\
          "This is free software, and you are welcome to redistribute it\n"\
          "under certain conditions.\n")
    
    plt.close("all")
    plt.rc("text", usetex = True)
    # plt.ion()

    fs, u = scipy.io.wavfile.read("../data/entradaP1.wav")
    fs, d = scipy.io.wavfile.read("../data/desejadoP1.wav")

    # convert to from int32 to float and normalize
    u = u.astype(float)/float(2**31)
    d = d.astype(float)/float(2**31)
    
    plt.figure(4)
    plt.plot(u, label = "u(i)")
    plt.plot(d, label = "d(i)")
    plt.legend()
    plt.savefig("../img/input_sigs.svg")
    
    # Simulation 1
    nW = 10
    mus = [1, 1e-1, 1e-2, 1e-3]

    runSims(nW, mus, u, d, name = "a")
    
    #plt.show()
    plt.close("all") 
    
    # Simulation 2
    nW  = 31
    mus = [1, 1e-1, 1e-2, 1e-3]

    runSims(nW, mus, u, d, name = "b")
    plt.close("all") 
    
    #plt.show()
    
    # Simulation 3
    
    nW  = 100
    mus = [1, 1e-1, 1e-2, 1e-3]

    runSims(nW, mus, u, d, name = "c")
    plt.close("all") 

    # Simulation 4
    nW = 4
    mus = [1, 1e-1, 1e-2, 1e-3]

    runSims(nW, mus, u, d, name = "d")
    plt.close("all") 
    
    #plt.show()
    
if (__name__ == "__main__"):
    main()
